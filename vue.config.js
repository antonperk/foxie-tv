const path = require('path');

module.exports = {
    pluginOptions: {
        electronBuilder: {
            mainProcessFile: path.resolve(
                __dirname,
                'src',
                'background',
                'main.ts'
            ),
            rendererProcessFile: path.resolve(
                __dirname,
                'src',
                'renderer',
                'main.ts'
            ),
            mainProcessTypeChecking: true,
            mainProcessWatch: [
                path.resolve(__dirname, 'src', 'background', '**', '*.ts'),
                path.resolve(__dirname, 'src', 'common', '**', '*.ts'),
            ],
            outputDir: 'dist',

            chainWebpackMainProcess(config) {
                config.resolve.alias.set(
                    '~',
                    path.resolve(__dirname, 'src', 'background')
                );
                config.resolve.alias.set(
                    'common',
                    path.resolve(__dirname, 'src', 'common')
                );
                config.resolve.alias.set('modules', path.resolve(__dirname, 'modules'));
            },

            chainWebpackRendererProcess(config) {
                config.resolve.alias.set(
                    '@',
                    path.resolve(__dirname, 'src', 'renderer')
                );
            },

            nodeIntegration: true,
        },
    },

    pages: {
        app: {
            entry: path.resolve(__dirname, 'src', 'renderer', 'main.ts'),
            template: path.resolve(__dirname, 'public', 'index.html'),
        },
    },

    indexPath: 'app.html',
    outputDir: 'dist',

    chainWebpack: (config) => {
        config.resolve.alias.set(
            'common',
            path.resolve(__dirname, 'src', 'common')
        );

        config.resolve.alias.set('modules', path.resolve(__dirname, 'modules'));
    },
};
