module.exports = {
    root: true,

    env: {
        node: true,
    },

    parserOptions: {
        ecmaVersion: 2020,
        parser: '@typescript-eslint/parser',
    },

    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'quotes': ['error', 'single'],
        'semi': ['error', 'always'],
        'object-curly-spacing': ['error', 'always'],
        'no-multi-spaces': ['error'],
        'comma-dangle': [
            'error',
            {
                arrays: 'always-multiline',
                objects: 'always-multiline',
                imports: 'only-multiline',
                exports: 'only-multiline',
                functions: 'never',
            },
        ],
        indent: 'off',
        'vue/script-indent': [
            'error',
            4,
            {
                baseIndent: 1,
            },
        ],
        '@typescript-eslint/no-var-requires': 'off',
        curly: 'error',
        '@typescript-eslint/no-explicit-any': 'off',
        'lines-between-class-members': [
            'error',
            'always',
            { exceptAfterSingleLine: true },
        ],
        'padded-blocks': ['error', { classes: 'always' }],
        'prettier/prettier': 'off',
    },

    extends: [
        'plugin:vue/essential',
        'eslint:recommended',
        '@vue/prettier',
        '@vue/prettier/@typescript-eslint',
        '@vue/typescript',
    ],
};
