import { Type, Container, Singleton } from 'common/di';
import { ModuleConfig } from 'common/module/config';

@Singleton
export class ModuleRegistry {

    protected registry: Map<Type<any>, ModuleConfig> = new Map();

    public register<T>(module: Type<T>, config: ModuleConfig = {}) {

        if (this.registry.has(module)) {
            return;
        }
        
        if ([...this.registry.values()].filter((c) => !!c.name).findIndex((c) => c.name === config.name) > -1) {
            throw new Error(`Module with name ${config.name} already defined.`);
        }

        Container.bind<T>(module).singleton();
        
        this.registry.set(module, config);
        
        // (config.injects || []).forEach((token) => {
        //     Container.bind(token);
        // });
    }

    public getModuleConfig<T>(module: Type<T>): ModuleConfig {
        if (!this.registry.has(module)) {
            throw new Error(`Undefined module ${module.name}. Did you register it wia @Module decorator?`);
        }

        return this.registry.get(module)!;
    }

    public getByName(name: string): Type<any> | undefined {
        const module = [...this.registry.entries()].find(([_, config]) => config.name === name);

        return module ? module[0] : undefined;
    }

    public getModules(): Map<Type<any>, ModuleConfig> {
        return this.registry;
    }

}