import { Type } from 'common/di/container';
import { AbstractProvider } from 'common/core/providers/declarations/provider';

export interface ModuleConfig {
    injects?: Array<Type<any>>;
    name?: string;
    providers?: Array<Type<AbstractProvider>>
}