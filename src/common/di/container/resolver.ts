import { ModuleConfig } from 'common/module';
import { Module } from 'vuex';
import { Container } from '../container';

export type Type<T> = new (...args: Array<any>) => T;

export type BindingFactory<T> = () => T;

export interface Binding<T> {
    target: any;
    concrete: Type<T>;
}

export interface Singleton<T> {
    target: Type<T>;
    concrete: T & Object;
}

export class Resolver<T> {

    protected factory?: BindingFactory<T>;

    protected concrete: Type<T> | T;
    
    protected _singleton: boolean = false;

    protected _module: boolean = false;

    protected _moduleConfig: ModuleConfig = {};

    constructor(concrete: Type<T>) {
        this.concrete = concrete;
    }

    public withFactory(factory: BindingFactory<T>): Resolver<T> {
        this.factory = factory;

        if (this._singleton) {
            this.concrete = this.factory();
        }

        return this;
    }

    public singleton(): void {
        if (this._singleton) {
            return;
        }

        if (this.factory) {
            this.concrete = this.factory();
        } else {
            this.concrete = Container.resolve(this.concrete as Type<T>);
        }

        this._singleton = true;
    }

    public isSingleton(): boolean {
        return this._singleton;
    }

    public isModule(): boolean {
        return this._module;
    }

    public hasFactory(): boolean {
        return !!this.factory;
    }

    public resolve(): T | Type<T> | BindingFactory<T> {
        return this.concrete;
    }

    public getFromFactory(): T {
        if (!this.factory) {
            throw new Error('Can\'t invoke factory for target');
        }

        return this.factory();
    }

}