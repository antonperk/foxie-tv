import 'reflect-metadata';
import { Type, Resolver } from 'common/di/container/resolver';

export class Container {

    protected static bindings: Map<Type<any>, Resolver<any>> = new Map();

    public static resolve<T>(target: Type<any>): T {
        if (!this.bindings.has(target)) {
            throw new Error(
                `Binding resolution exception. Can't resolve target ${target.name}`
            );
        }

        const resolver = this.bindings.get(target)!;

        if (resolver.isSingleton()) {
            return resolver.resolve();
        }

        if (resolver.hasFactory()) {
            return resolver.getFromFactory();
        }

        const concrete = resolver.resolve();

        const metadata =
            Reflect.getMetadata('design:paramtypes', concrete) || [];
        const params = metadata.map((token) => {
            return Container.resolve(token);
        });

        return new concrete(...params) as T;
    }

    public static bind<T>(
        target: Type<any>,
        concrete: Type<T> = target,
        force: boolean = false
    ) {
        if (!this.bindings.has(target) || !force) {
            const resolver = new Resolver<T>(concrete);
            this.bindings.set(target, resolver);
        }

        return this.bindings.get(target)! as Resolver<T>;
    }

    public static getResolver<T>(target: Type<any>): Resolver<T> | undefined {
        return this.bindings.get(target);
    }

}

export { Type } from './container/resolver';
