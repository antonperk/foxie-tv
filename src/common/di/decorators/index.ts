import { Type } from 'common/di/container/resolver';
import { Container } from 'common/di/container';
import { ModuleConfig } from 'common/module';
import { ModuleRegistry } from 'common/module/registry';
import { AbstractProvider } from 'common/core/providers/declarations/provider';
import { AppContract } from 'common/core/app';
import { AppConfig } from 'common/core/app/config';

export function Injectable(target: Type<any>) {
    Container.bind(target);
}

export function Singleton(target: Type<any>) {
    Container.bind(target).singleton();
}

export function Module(config: ModuleConfig = {}) {
    return function (target: Type<any>) {
        const registry = Container.resolve<ModuleRegistry>(ModuleRegistry);
    };
}

export function Provider(target: Type<AbstractProvider>) {
    Container.bind(target)
        .withFactory(() => {
            return new target(Container.resolve(AppContract as Type<AppContract>));
        })
        .singleton();
}

export function AppEntry<T extends AppContract>(config: AppConfig) {
    return (target: Type<T>) => {
        
    };
}