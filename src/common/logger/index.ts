import moment from 'moment';

export class Logger {

    constructor(protected context?: string) {}

    public log(message: any, type: 'info' | 'warn' | 'error' = 'info'): void {
        let prefix: string = `[${moment().format('Y-m-d H:M:s')}] [${type.toUpperCase()}]`;
        let color: string;

        if (this.context) {
            prefix += ` [${this.context}] `;
        }

        switch (type) {
            case 'info':
                color = '#2ECC71';
                break;
            case 'error':
                color = '#E74C3C';
                break;
            case 'warn':
                color = '#F4D03F';
                break;
        }

        message = `%c${prefix} ${message}`;

        switch(type) {
            case 'info':
                console.log(message, color);
                break;
            case 'error':
                console.error(message, color);
                break;
            case 'warn':
                console.warn(message, color);
        }
    }

    public warn(message: any): void {
        this.log(message, 'warn');
    }

    public error(message: any): void {
        this.log(message, 'error');
    }

}