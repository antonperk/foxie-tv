import { App as BackgroundApp } from '@/core/app';
import { App as RendererApp } from '~/core/app';

export type AppCallback = (app: BackgroundApp | RendererApp) => Promise<void>;
