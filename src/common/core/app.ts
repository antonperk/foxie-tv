import { AbstractProvider } from 'common/core/providers/declarations/provider';
import { Container, Type } from 'common/di';
import { Logger } from 'common/logger';

export abstract class AppContract {

    protected logger: Logger = new Logger('App');

    public boot(appModule: Type<any>): void {

    }

    public bootProvider(provider: Type<AbstractProvider>): void {
        const instance = Container.resolve<AbstractProvider>(provider);
        
        if (instance.isBooted()) {
            return;
        }

        instance.boot();
        instance.booted();

        this.logger.log(`${provider.name} provider booted.`);

        if (instance.provides.length > 0) {
            instance.provides().forEach((childProvider) => {
                this.bootProvider(childProvider);
            });
        }
    }

}