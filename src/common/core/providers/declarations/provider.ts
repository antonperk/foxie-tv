import { App as BackgroundApp } from '~/core/app';
import { App as RendererApp } from '@/core/app';
import { Type } from 'common/di';

export abstract class AbstractProvider {

    protected _booted: boolean = false;

    constructor(protected app: BackgroundApp | RendererApp) {
        
    }

    public boot(): void {

    }

    public register(): void {

    }

    public provides(): Array<Type<AbstractProvider>> {
        return [];
    }

    public isBooted(): boolean {
        return this._booted;
    }

    public booted(): void {
        this._booted = true;
    }

}