import { Container, Singleton, Type } from 'common/di';
import { App } from '~/core/app';
import { AbstractProvider } from './declarations/provider';

@Singleton
export class ProviderRepository {

    protected repository: Array<Type<AbstractProvider>> = [];

    constructor(protected app: App) {}

    public boot() {        
        this.repository.forEach((provider) => {
            this.app.bootProvider(provider);
        });
    }

    public addProvider(provider: Type<AbstractProvider>) {
        this.repository.push(provider);
    }

}