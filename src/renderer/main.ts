import { App } from '@/core/app';
import { AppContract } from 'common/core/app';
import { Container, Type } from 'common/di';

Container.bind<App>(AppContract as Type<App>)
    .withFactory(() => {
        return new App(__dirname);
    })
    .singleton();

const app = Container.resolve<App>(AppContract as Type<App>);

app.boot();