import path from 'path';
import { EventEmitter } from 'events';
import { AppContract } from 'common/core/app';
import { AppEntry } from 'common/di/decorators';

@AppEntry({
    type: 'renderer',
})
export class App extends AppContract {

    protected booted: boolean = false;

    protected events: EventEmitter = new EventEmitter();

    constructor(protected basePath: string) {
        super();
    }

    public path(location: string = ''): string {
        return path.resolve(this.basePath, location);
    }

    public isBooted(): boolean {
        return this.booted;
    }

    public boot(): App {
        if (this.isBooted()) {
            throw new Error('Application was already booted');
        }

        this.booted = true;
        this.emit('booted');

        return this;
    }

    public on(token: string, fn: (...args: Array<any>) => void) {
        this.events.on(token, fn);
    }

    protected emit(token: string, data?: any) {
        this.events.emit(token, data);
    }

}
