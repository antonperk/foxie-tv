import { App } from '~/core/app';
import { Container, Type } from 'common/di';
import { app as electronApp } from 'electron';
import { AppContract } from 'common/core/app';

Container.bind<App>(AppContract as Type<App>)
    .withFactory(() => {
        return new App(__dirname, electronApp);
    })
    .singleton();

const app = Container.resolve<App>(AppContract as Type<App>);

/**
 * Using require here because the module itself will be decorated 
 * BEFORE the app was even initilized
 */
app.boot(require('~/core/module/app.module').AppModule);


console.log(Container);