import path from 'path';
import { EventEmitter } from 'events';
import { App as ElectronApp, protocol, BrowserWindow } from 'electron';
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib';
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer';
import { AppContract } from 'common/core/app';
import { AppEntry } from 'common/di/decorators';
import { AppCallback } from 'common/core/app/hooks';
import { Container, Type } from 'common/di';

@AppEntry({
    type: 'background',
})
export class App extends AppContract {

    protected booted: boolean = false;

    protected events: EventEmitter = new EventEmitter();

    protected window!: BrowserWindow;

    protected _beforeBoot: Array<AppCallback> = [];

    protected _afterBoot: Array<AppCallback> = [];

    protected mainModule!: Type<any>;

    constructor(protected basePath: string, protected app: ElectronApp) { 
        super();
    }

    public beforeBoot(callback: AppCallback) {
        this._beforeBoot.push(callback);
    }

    public afterBoot(callback: AppCallback) {
        this._afterBoot.push(callback);
    }

    public path(location: string = ''): string {
        return path.resolve(this.basePath, location);
    }

    public isBooted(): boolean {
        return this.booted;
    }

    public async boot(appModule: Type<any>): Promise<App> {
        if (this.isBooted()) {
            throw new Error('Background process was already booted up');
        }

        this.logger.log('Running beforeBoot hooks...');
        
        const beforeBootPromises: Array<Promise<void>> = [];
        this._beforeBoot.forEach((hook) => {
            beforeBootPromises.push(hook(this));
        });

        await Promise.all(beforeBootPromises);

        this.logger.log('beforeBoot hooks executed successfully.');

        this.logger.log('Binging Electron events...');

        protocol.registerSchemesAsPrivileged([
            { scheme: 'app', privileges: { secure: true, standard: true } },
        ]);

        this.app.on('window-all-closed', () => {
            if (process.platform !== 'darwin') {
                this.app.quit();
            }
        });

        this.app.on('activate', async () => {
            if (BrowserWindow.getAllWindows().length === 0) {
                this.window = await this.createWindow();
            }
        });

        this.app.on('ready', async () => {
            if (this.isDevelopment() && !process.env.IS_TEST) {
                // Install Vue Devtools
                try {
                    await installExtension(VUEJS_DEVTOOLS);
                } catch (e) {
                    console.error('Vue Devtools failed to install:', e.toString());
                }
            }

            this.window = await this.createWindow();
        });

        if (this.isDevelopment()) {
            if (process.platform === 'win32') {
                process.on('message', (data) => {
                    if (data === 'graceful-exit') {
                        this.app.quit();
                    }
                });
            } else {
                process.on('SIGTERM', () => {
                    this.app.quit();
                });
            }
        }

        this.booted = true;

        this.logger.log('Running afterBoot hooks...');

        const afterBootHooksPromises: Array<Promise<void>> = [];
        this._beforeBoot.forEach((hook) => {
            afterBootHooksPromises.push(hook(this));
        });
        await Promise.all(afterBootHooksPromises);

        this.logger.log('afterBoot hooks executed successfully.');

        this.emit('booted');

        this.logger.log('Kernel booted successfully!');

        this.logger.log('Invoking base app module...');
        
        Container.resolve(appModule);

        return this;
    }

    protected async createWindow(): Promise<BrowserWindow> {
        const window = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                nodeIntegration: (process.env.ELECTRON_NODE_INTEGRATION as unknown) as boolean,
            },
        });

        if (process.env.WEBPACK_DEV_SERVER_URL) {
            await window.loadURL((process.env.WEBPACK_DEV_SERVER_URL as string) + '/app.html');

            if (!process.env.IS_TEST) {
                window.webContents.openDevTools();
            }
        } else {
            createProtocol('app');
            window.loadURL('app://./app.html');
        }

        return window;
    }

    protected emit(token: string, data?: any) {
        this.events.emit(token, data);
    }

    public on(token: string, handler: (...args: Array<any>) => void) {
        this.events.on(token, handler);
    }

    public isDevelopment(): boolean {
        return process.env.NODE_ENV !== 'production';
    }

    public getApp(): ElectronApp {
        return this.app;
    }

    public getWindow(): BrowserWindow {
        return this.window;
    }

}