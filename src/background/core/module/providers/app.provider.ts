import { AbstractProvider } from 'common/core/providers/declarations/provider';
import { Provider } from 'common/di';

@Provider
export class AppProvider extends AbstractProvider {

}