import { Module } from 'common/di';
import { AppProvider } from '~/core/module/providers/app.provider';

@Module({
    providers: [AppProvider],
})
export class AppModule {

    constructor() {
        console.log(this);
    }

}